@echo off
rmdir /s/q dist
rmdir /s/q i18n
del /s/q package.json

mklink /J dist "..\..\..\..\ifdef-creator\dist"
mklink /J i18n "..\..\..\..\ifdef-creator\i18n"
mklink /H package.json "..\..\..\..\ifdef-creator\package.json"
pause