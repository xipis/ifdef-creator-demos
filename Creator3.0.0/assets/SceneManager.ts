
import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;


//尝试用插件“选择”外部模块（即npm模块），可以查看打包输出中是否包含不需要的外部模块代码
/// #if MiniApp
//用微信小游戏做测试，看“build\wechatgame\src\chunks\bundle.js”文件，看是否只打包了tsrpc-miniapp，而没有tsrpc-browser
/// #code import { HttpClient, WsClient } from 'tsrpc-miniapp';
/// #elif MiniApp_browser
//用同样微信小游戏的构建环境，看“build\wechatgame-browser\src\chunks\bundle.js”文件，看是否只打包了tsrpc-browser ，而没有tsrpc-miniapp
/// #code import { HttpClient, WsClient } from 'tsrpc-browser';
/// #else
import { HttpClient, WsClient } from 'tsrpc-browser';
/// #endif

@ccclass('SceneManager')
export class SceneManager extends Component {
    start() {

        //简单使用的条件编译

        /// #if Web
        console.log("web环境");
        /// #endif

        /// #if Mobile
        console.log("移动端环境");
        /// #elif Desktop
        console.log("桌面环境");
        /// #endif

        //组合条件表达式 和 条件嵌套

        /// #if Web && Mobile
        ///     #code var testMsg2 = "H5-防止语法错误可以把代码放到code语句块里";
        ///     #if SDK1
        ///         #code console.log("H5里定义了SDK1,本条日志写在#code语句段中");
        ///     #elif SDK2
        console.log("H5里定义了SDK2");
        ///     #endif
        /// #elif Web && Desktop
        ///     #code var testMsg2="PC网页-防止语法错误可以把代码放到code语句块里";
        ///     #if SDK1
        ///         #code console.log("PC网页里定义了SDK1,本条日志写在#code语句段中");
        ///     #elif SDK2
        ///         #code console.log("PC网页里定义了SDK2,本条日志写在#code语句段中");
        ///     #endif
        /// #else
        var testMsg2 = "其他";
        /// #endif
        console.log(testMsg2);
        
        console.log("typeof(HttpClient):",typeof(HttpClient));
    }
}