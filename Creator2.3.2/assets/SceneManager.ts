// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    start () {
        //简单使用的条件编译

        /// #if Web
        console.log("web环境");
        /// #endif

        /// #if Mobile
        console.log("移动端环境");
        /// #elif Desktop
        console.log("桌面环境");
        /// #endif

        //组合条件表达式 和 条件嵌套

        /// #if Web && Mobile
        ///     #code var testMsg2 = "H5-防止语法错误可以把代码放到code语句块里";
        ///     #if SDK1
        ///         #code console.log("H5里定义了SDK1,本条日志写在#code语句段中");
        ///     #elif SDK2
        console.log("H5里定义了SDK2");
        ///     #endif
        /// #elif Web && Desktop
        ///     #code var testMsg2="PC网页-防止语法错误可以把代码放到code语句块里";
        ///     #if SDK1
        ///         #code console.log("PC网页里定义了SDK1,本条日志写在#code语句段中");
        ///     #elif SDK2
        ///         #code console.log("PC网页里定义了SDK2,本条日志写在#code语句段中");
        ///     #endif
        /// #else
        var testMsg2 = "其他";
        /// #endif
        console.log(testMsg2);
    }
}
